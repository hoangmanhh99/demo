//
//  APIClient+Authentication.swift
//  Solucall
//
//  Created by Tu Tran on 7/6/20.
//

import Foundation
import Moya
import SwiftyJSON

extension APIClient {
    static func login(email: String, password: String, completion: @escaping RequestSuccessFailCompletion) {
        let target = MultiTarget(ResourcesAPI.login(email: email, password: password))
        NetworkAdapter.request(target: target) { (response, contentArray, error) in
//            if let error = error {
//                completion(false, error)
//            } else {
//                if let json = contentArray?.first {
//                    completion(true, nil)
//                } else {
//                    completion(false, nil)
//                }
//            }
            if let error = error {
                completion(true, error)
            } else {
                completion(true, error)
            }
        }
    }
    
    static func getListPosts(completion: @escaping RequestListCompletion) {
        
        let target = MultiTarget(ResourcesAPI.posts)
        NetworkAdapter.request(target: target) { (response, contentArray, error) in
            if let error = error {
                completion(nil, error)
            } else {
                if let jsons = contentArray {
                    var results: [Post] = []
                    for json in jsons {
                        results.append(Post(json))
                    }
                    completion(results, nil)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
    
    static func getDetailPost(_ postId: Int, completion: @escaping RequestDetailItemCompletion) {
        
        let target = MultiTarget(ResourcesAPI.post(postId))
        NetworkAdapter.request(target: target) { (response, contentArray, error) in
            if let error = error {
                completion(nil, error)
            } else {
                if let json = contentArray?.first {
                    completion(Post(json), nil)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
    
    static func getCompanyUsers(completion: @escaping RequestDetailItemCompletion) {
        let target = MultiTarget(ResourcesAPI.users)
        NetworkAdapter.request(target: target) { (respone, contentArray, error) in
            if let error = error {
                completion(nil, error)
            } else {
                if let json = contentArray?.first {
                    completion(Company(json), nil)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
    
    static func getListPhotos(completion: @escaping RequestListCompletion) {
        
        let target = MultiTarget(ResourcesAPI.photos)
        NetworkAdapter.request(target: target) { (response, contentArray, error) in
            if let error = error {
                completion(nil, error)
            } else {
                if let jsons = contentArray {
                    var results: [Photo] = []
                    for json in jsons {
                        results.append(Photo(json))
                    }
                    completion(results, nil)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
}
