//
//  AuthenticationAPI.swift
//  Solucall
//
//  Created by Tu Tran on 7/6/20.
//

import Foundation
import UIKit
import Moya

enum ResourcesAPI {
    case posts
    case post(_ postId: Int)
    case comments
    case albums
    case photos
    case todos
    case users
    case login(email: String, password: String)
}

extension ResourcesAPI: TargetType {
    var baseURL: URL {
        return URL(string: SERVER_BASE_URL)!
    }
    
    var path: String {
        switch self {
        case .posts:
            return "/posts"
        case .post(let postId):
            return "/posts/\(postId)"
        case .comments:
            return "/comments"
        case .albums:
            return "/albums"
        case .photos:
            return "/photos"
        case .todos:
            return "/todos"
        case .users:
            return "/users"
        case .login(_,_):
            return "/login"
        }
    }
    
    var method: Moya.Method {
//        switch self {
//        case .login(_,_):
//            return .post
//        default:
        return .get
        
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .login(let email, let password):
            var params = [String: Any]()
            params["email"] = email
            params["password"] = password
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        default:
            return .requestPlain
        } 
    }
    
    var headers: [String : String]? {
        return MakeDefaultHeader()
    }
    
    
}

extension ResourcesAPI: MoyaCacheable {
    var cachePolicy: MoyaCacheablePolicy {
        switch self {
        default:
            return .reloadIgnoringLocalCacheData
        }
    }
}


