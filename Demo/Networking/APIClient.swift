//
//  APIClient.swift
//  LetsPlay360
//
//  Created by Quang Tran on 10/17/19.
//  Copyright © 2019 quangtran. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON

typealias RequestSuccessFailCompletion = (_ success: Bool, _ error: LPError?) -> Void
typealias RequestListCompletion = (_ items: [Any]?, _ error: LPError?) -> Void
typealias RequestDetailItemCompletion = (_ item: Any?, _ error: LPError?) -> Void

func MakeDefaultHeader() -> [String: String] {
    let timezone = TimeZone.current.identifier
//    if let accessToken = Global.shared.accessToken {
//        let headers = [
//            "Content-Type": "application/json",
//            "Authorization": "Bearer \(accessToken)",
//            "cache-control": "no-cache",
//            "Timestamp": "\(Int(Date().timeIntervalSince1970))",
//            "Timezone": timezone,
//            "App-Token": TemplateAppManager.shared.getToken()
//        ]
//        return headers
//    } else {
        
        let headers = [
            "Content-Type": "application/json",
            "cache-control": "no-cache",
            "Timestamp": "\(Int(Date().timeIntervalSince1970))",
            "Timezone": timezone,
        ]
        return headers
//    }
    
}

protocol MoyaCacheable {
    typealias MoyaCacheablePolicy = URLRequest.CachePolicy
    var cachePolicy: MoyaCacheablePolicy { get }
}

final class MoyaCacheablePlugin: PluginType {
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        if let moyaCachableProtocol = target as? MoyaCacheable {
            var cachableRequest = request
            cachableRequest.cachePolicy = moyaCachableProtocol.cachePolicy
            return cachableRequest
        }
        return request
    }
}

class APIClient {
    static let shared = APIClient()
    init() {
        let MEMORY_CAPACITY = 4 * 1024 * 1024
        let DISK_CAPACITY =  20 * 1024 * 1024
        
        let cache = URLCache(memoryCapacity: MEMORY_CAPACITY, diskCapacity: DISK_CAPACITY, diskPath: nil)
        URLCache.shared = cache
    }
}
