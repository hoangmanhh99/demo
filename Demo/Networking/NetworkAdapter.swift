//
//  NetworkAdapter.swift
//  LetsPlay360
//
//  Created by Quang Tran on 10/17/19.
//  Copyright © 2019 quangtran. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

let SERVER_BASE_URL = "https://605871d0c3f49200173ae204.mockapi.io/api/demo"

let TIMEOUT_REQUEST: Double = 120.0

enum ErrorCode: Int {
    case unknown = 10000
    case connection = 9999
    case expiredToken = 401
    case itemNotFound = 404

    var success: Bool {
        return false
    }
    
    var message: String {
        switch self {
        default:
            return ""
        }
    }
    
}

struct LPError {
    var statusCode: Int
    var success: Bool
    var message: String
    var data: [String: Any]?
    
    init(code: ErrorCode) {
        self.statusCode = code.rawValue
        self.success = code.success
        self.message = code.message
    }
    
    init(json: JSON, statusCode: Int) {
        self.statusCode = statusCode
        self.success = json["success"].boolValue
        self.message = json["message"].stringValue
        self.data = json["data"].dictionaryObject
    }
    
    func isExistKey(_ key: String) -> Bool {
        if let _ = self.data?[key] as? Any {
            return true
        }
        return false
    }
    
}

typealias RequestCompletion = (_ response: Response?, _ arrayContent: [JSON]?, _ error: LPError?) -> Void

func ParseResponse(_ target: MultiTarget,
                   _ response: Response?,
                   completion: @escaping RequestCompletion) {
    
    if let data = response?.data {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) else {
            completion(response, nil, nil)
            return
        }
        print("========= Success \(target.method.rawValue) \(target.path) ============\n \(json)")
        let jsonValue = JSON(json as Any)
        var array: [JSON]? = jsonValue.array
        if let dict = jsonValue.dictionary, array == nil {
            array = [JSON(dict)]
        }
//        var array: [JSON]? = jsonValue["data"].array
//        if let dict = jsonValue["data"].dictionary, array == nil {
//            array = [JSON(dict)]
//        }
        //Some paths return only success: true/false. None of data in response object
        let paths: [String] = ["validate_timeslot", "/geocode/json"]
        if !paths.filter({ return target.path.hasSuffix($0) }).isEmpty {
            array = [jsonValue]
        }
        completion(response, array, nil)
        return
    }
    completion(response, nil, LPError(code: .unknown))
}

func ParseError(_ target: MultiTarget,
                _ response: Response?,
                completion: @escaping RequestCompletion) {
    if let data = response?.data {
        guard let json = try? JSON(data: data, options: .allowFragments) else {
            completion(response, nil, LPError(code: .unknown))
            return
        }
        print("========= Error \(target.method.rawValue) \(target.path) ============\n \(json)")
        let error = LPError(json: json, statusCode: response?.statusCode ?? 0)
        completion(response, nil, error)
        return
    }
    completion(response, nil, LPError(code: .unknown))
}

struct NetworkAdapter {
    static let provider = MoyaProvider<MultiTarget>(plugins: [MoyaCacheablePlugin()])
    static func request(target: MultiTarget,
                        completion: @escaping RequestCompletion) {
        print("========= Request \(target.method.rawValue) \(target.path) ============\n \(target.task)")
        provider.session.sessionConfiguration.timeoutIntervalForRequest = TIMEOUT_REQUEST
        provider.request(target) { (result) in
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    ParseResponse(target, response, completion: completion)
                } else if response.statusCode == 404 {
                    //Item not found
                    completion(response, nil, LPError(code: .itemNotFound))
                } else {
                    // Show error from server
                    ParseError(target, response, completion: { (response, contentArray, error) in
                        if let error = error {
                            if error.statusCode == ErrorCode.expiredToken.rawValue {
                                completion(nil, nil, error)
                            } else {
                                completion(nil, nil, error)
                            }
                        } else {
                            completion(nil, nil, error)
                        }
                    })
                }
            case .failure(let error):
                // Show Moya error
                print("========= Error \(target.method.rawValue) \(target.path) ============\n \(error)")
                ParseError(target, nil, completion: completion)
            }
        }
    }
    
    static func cancel() {
        provider.session.session.getAllTasks { (tasks) in
            tasks.forEach({ $0.cancel() })
        }
    }
}

