//
//  ContentPosts.swift
//  Demo
//
//  Created by MacOS on 22/03/2021.
//

import Foundation
import Moya
import SwiftyJSON

class BaseItem: NSObject {
    var id: Int = 0
    var name: String = ""
    
    init(_ json: JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
    }
}

class User: BaseItem {
    var username: String = ""
    var email: String = ""
    var phone: String = ""
    var website: String = ""
    
    var address: Address?
    var company: Company?
    
    override init(_ json: JSON) {
        super.init(json)
        self.username = json["username"].stringValue
        self.email = json["email"].stringValue
        self.phone = json["geo"]["phone"].stringValue
        self.website = json["geo"]["website"].stringValue
        if json["address"].exists() && json["address"] != JSON.null {
            self.address = Address(json["address"])
        }
        if json["company"].exists() && json["company"] != JSON.null {
            self.company = Company(json["company"])
        }
    }
}

class Address: BaseItem {
    var street: String = ""
    var suite: String = ""
    var zipcode: String = ""
    var geo: GeoLocation?
    
    override init(_ json: JSON) {
        super.init(json)
        self.street = json["street"].stringValue
        self.suite = json["suite"].stringValue
        self.zipcode = json["zipcode"].stringValue
        
        if json["geo"].exists() && json["geo"] != JSON.null {
            self.geo = GeoLocation(json["geo"])
        }
    }
}

class Company: BaseItem {
    var nameCom: String = ""
    var catchPhrase: String = ""
    var bs: String = ""
    
    override init(_ json: JSON) {
        super.init(json)
        self.nameCom = json["company"]["name"].stringValue
        self.catchPhrase = json["company"]["catchPhrase"].stringValue
        self.bs = json["company"]["bs"].stringValue
    }
}

class GeoLocation: BaseItem {
    var lat: Double = 0.0
    var lng: Double = 0.0

    override init(_ json: JSON) {
        super.init(json)
        self.lat = json["lat"].doubleValue
        self.lng = json["lng"].doubleValue
    }
}
