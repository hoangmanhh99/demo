//
//  Posts.swift
//  Demo
//
//  Created by MacOS on 19/03/2021.
//

import Foundation
import Moya
import SwiftyJSON

class Post: NSObject {
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var body: String = ""
    
    init(_ json: JSON) {
        self.userId = json["userId"].intValue
        self.id = json["id"].intValue
        self.title = json["title"].stringValue
        self.body = json["body"].stringValue
    }
}



