//
//  Photos.swift
//  Demo
//
//  Created by MacOS on 22/03/2021.
//

import Foundation
import SwiftyJSON

class Photo: NSObject {
    var albumId: Int = 0
    var id: Int = 0
    var title: String = ""
    var url: String = ""
    var thumbnailUrl: String = ""
    
    init(_ json: JSON) {
        self.albumId = json["albumId"].intValue
        self.id = json["id"].intValue
        self.title = json["title"].stringValue
        self.url = json["url"].stringValue
        self.thumbnailUrl = json["thumbnailUrl"].stringValue
    }
}
