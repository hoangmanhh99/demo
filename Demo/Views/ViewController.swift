//
//  ViewController.swift
//  Demo
//
//  Created by User on 01/03/2021.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var contentConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var contentImage: UIImageView!
    @IBOutlet weak var contentCollectionView: UICollectionView!
    
    @IBOutlet weak var imagePageController: UIPageControl!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    let pics = ["paperplane", "map", "person"]
    
    var titles = ["Berichtgeving", "Locatievoorziening", "Maak een account aan"]
    var contents = ["Schakel berichtgeving in zodat we u op de hoogte kunnel houden van uw bestelligen en promoties", "Schakel berichtgeving in zodat we u op de hoogte kunnel houden van uw ", "Schakel berichtgeving in zodat we u op de hoogte kunnel houden van uw bestelligen"]
    
    let screenSize: CGRect = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        let nib = UINib(nibName: "ContentCollectionViewCell", bundle: .main)
//        contentCollectionView.register(nib, forCellWithReuseIdentifier: "cell")
        if screenSize.height < 600 {
            contentConstraint.constant = 40
            contentStackView.spacing *= 0.1
            contentImage.bounds.size.width *= 0.6
            
            titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
            subtitleLabel.font = UIFont.systemFont(ofSize: 11)
        } else if screenSize.height < 800 {
            contentStackView.spacing *= 0.4
            contentImage.bounds.size.width *= 0.8
            
            titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
            subtitleLabel.font = UIFont.systemFont(ofSize: 12)
        }
        
        print(titles[1])

        contentView.layer.cornerRadius = 30
        contentView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        nextButton.layer.cornerRadius = 10
        
        imagePageController.numberOfPages = pics.count
        imagePageController.currentPage = 0
        
        contentImage.image = UIImage(named: "ready")
        
        updateCenterItem()
        
//        APIClient.getDetailPost(1) { (item, _) in
//            guard let post = item as? Post else { return }
//            print(post)
//        }
//        APIClient.getListPhotos { (items, _) in
//            guard let item = items as? [Photo] else { return }
//            print(item.count)
//        }
        
        APIClient.login(email: "Kendall_Kuvalis@yahoo.co1", password: "QkA2FB40P5Jqkf1") { (items, _) in
            
            print(items)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = contentCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ContentCollectionViewCell else { return UICollectionViewCell() }
        cell.contentImageView.image = UIImage(named: pics[indexPath.row])
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x / contentCollectionView.frame.size.width)
        imagePageController.currentPage = index
        
        updateCenterItem()
    }

    @IBAction func nextBtn(_ sender: Any) {
        imagePageController.currentPage += 1
        contentCollectionView.scrollToItem(at: IndexPath(item: imagePageController.currentPage, section: 0), at: .centeredHorizontally, animated: true)
        updateCenterItem()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let mainVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
        self.navigationController?.pushViewController(mainVC, animated: true)
//        guard let mainVC = storyboard.instantiateViewController(withIdentifier: "PostViewController") as? PostViewController else { return }
//        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    
    private func updateCenterItem() {
        titleLabel.text = titles[imagePageController.currentPage]
        subtitleLabel.text = contents[imagePageController.currentPage]
    }
    
}
// MARK: Custom CollectionView
extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
}



