//
//  NotiTableViewCell.swift
//  Demo
//
//  Created by MacOS on 03/03/2021.
//

import UIKit

let reusableCell = "cell"

protocol SwitchDelegate {
    func updateSwitch(_ sender: NotiTableViewCell, _ valueChanged: Bool)
}

class NotiTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleNotiLabel: UILabel!
    @IBOutlet weak var contentNotiLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var onOffSwitch: UISwitch!
    
    var delegate: SwitchDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        onOffSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        contentNotiLabel.text = ""
        timeLabel.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func stateSwitch(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.updateSwitch(self, onOffSwitch.isOn)
        }
    }
}
