//
//  ContentCollectionViewCell.swift
//  Demo
//
//  Created by User on 01/03/2021.
//

import UIKit

class ContentCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var contentImageView: UIImageView!
    
    let screenSize: CGRect = UIScreen.main.bounds

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if screenSize.height < 600 {
            contentImageView.bounds.size.width *= 0.6
        } else if screenSize.height < 800 {
            contentImageView.bounds.size.width *= 0.8
        }
    }

}
