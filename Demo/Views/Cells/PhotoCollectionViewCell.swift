//
//  PhotoCollectionViewCell.swift
//  Demo
//
//  Created by MacOS on 22/03/2021.
//

import UIKit
import Kingfisher

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    public static var identifier: String {
        return "PhotoCollectionViewCell"
    }
    
    public func configureWith(_ photo: Photo) {
        titleLabel.text = photo.title
        contentImageView.kf.setImage(with: URL(string: photo.url))
    }

}
