//
//  CreateProfileTableViewCell.swift
//  Demo
//
//  Created by MacOS on 09/03/2021.
//

import UIKit

let reusableProfileCell = "cell"

class CreateProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var expandBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 23
        mainView.layer.borderWidth = 2
        mainView.layer.borderColor = UIColor(rgb: 0xE9EBFF).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
