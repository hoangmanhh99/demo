//
//  PostTableViewCell.swift
//  Demo
//
//  Created by MacOS on 22/03/2021.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    public static var identifier: String {
        return "PostTableViewCell"
    }
    
    public func configureWith(_ post: Post) {
        titleLabel.text = post.title
        bodyLabel.text = post.body 
    }

}
