//
//  PostViewController.swift
//  Demo
//
//  Created by MacOS on 22/03/2021.
//

import UIKit

class PostViewController: UIViewController {
    @IBOutlet weak var postTableView: UITableView!
    
    var posts : [Post] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initData()
    }

    func initData() {
        APIClient.getListPosts { (items, _) in
            guard let items = items as? [Post] else { return }
            self.posts.removeAll()
            self.posts.append(contentsOf: items)
            DispatchQueue.main.async {
                self.postTableView.reloadData()
            }
        }
    }
}

extension PostViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PostTableViewCell.identifier) as? PostTableViewCell else { return UITableViewCell() }
        cell.configureWith(self.posts[indexPath.row])
        return cell
    }
    
    
}
