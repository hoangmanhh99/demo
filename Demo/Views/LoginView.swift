//
//  LoginView.swift
//  Demo
//
//  Created by MacOS on 15/03/2021.
//

import UIKit
import Moya

class LoginView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    
    var repos = NSArray()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        Bundle.main.loadNibNamed("LoginView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        contentView.layer.cornerRadius = 30
        contentView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        emailTextField.layer.masksToBounds = true
        emailTextField.layer.cornerRadius = 10
        passwordTextField.layer.masksToBounds = true
        passwordTextField.layer.cornerRadius = 10
        loginBtn.layer.cornerRadius = 10
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        print("1")
        APIClient.login(email: emailTextField.text!, password: passwordTextField.text!) { (_, _) in
            print()
        }
            
        if emailTextField.text == "a" && passwordTextField.text == "b" {
            let alert = UIAlertController(title: "Authentication", message: "Login successfully", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
}
