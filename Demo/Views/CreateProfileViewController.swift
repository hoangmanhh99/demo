//
//  CreateProfileViewController.swift
//  Demo
//
//  Created by MacOS on 09/03/2021.
//

import UIKit

enum TitleProfile {
    case medicalHistory
    case medication
    case allergies
    case emergencyContact
    case familyDoctor
    case familyDentist
    case dependants
    case relationshipStatus
    case otherLink
    case socialLinks
}

extension TitleProfile {
    var title: String? {
        switch self {
        case .medicalHistory:
            return "Medical History"
        case .medication:
            return "Medication"
        case .allergies:
            return "Allergies"
        case .emergencyContact:
            return "Emergency Contact"
        case .familyDoctor:
            return "Family Doctor"
        case .familyDentist:
            return "Family Dentist"
        case .dependants:
            return "Dependants"
        case .relationshipStatus:
            return "Relationship Status"
        case .otherLink:
            return "Other Link"
        case .socialLinks:
            return "Social Links"
        }
    }
    
    var placeholder: String? {
        switch self {
        case .medicalHistory:
            return "Add medical history"
        case .medication:
            return "Add medication"
        case .allergies:
            return "Add allergies"
        case .emergencyContact:
            return "Add an amergency contact"
        case .familyDoctor:
            return "Add a family doctor"
        case .familyDentist:
            return "Add a family dentist"
        case .dependants:
            return "Add dependants"
        case .relationshipStatus:
            return "Select your relationship status"
        case .otherLink:
            return "Add other link"
        case .socialLinks:
            return "Add social links"
        }
    }
    
    var addContent: UIImage? {
        switch self {
        case .relationshipStatus:
            return UIImage(named: "down")
        case .socialLinks:
            return UIImage(named: "plus")
        default:
            return nil
        }
    }
}

class CreateProfileViewController: UIViewController {
    
    @IBOutlet weak var profileTableView: UITableView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var cameraImageView: UIButton!
    @IBOutlet weak var nameTextView: UITextView!
    @IBOutlet weak var desTextView: UITextView!
    @IBOutlet weak var addProfileBtn: UIButton!
    
    
    private var titles: [TitleProfile] = [.medicalHistory, .medication, .allergies, .emergencyContact, .familyDoctor, .familyDentist, .dependants, .relationshipStatus, .otherLink, .socialLinks]
    
    public var imagePickerController: UIImagePickerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "CreateProfileTableViewCell", bundle: .main)
        profileTableView.register(nib, forCellReuseIdentifier: reusableProfileCell)
        
        profileTableView.separatorStyle = .none
        addProfileBtn.layer.cornerRadius = 23
        
        avatarImageView.image = UIImage(named: "avatar")
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
    }
    
    @IBAction func selectImageBtn(_ sender: Any) {
        self.imagePickerController = UIImagePickerController.init()
        
        let alert = UIAlertController.init(title: "Select Source Type", message: nil, preferredStyle: .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            alert.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { (_) in
                self.presentImagePicker(controller: self.imagePickerController!, source: .camera)
            }))
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            alert.addAction(UIAlertAction.init(title: "Photo Library", style: .default, handler: { (_) in
                self.presentImagePicker(controller: self.imagePickerController!, source: .photoLibrary)
            }))
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            alert.addAction(UIAlertAction.init(title: "Saved Albums", style: .default, handler: { (_) in
                self.presentImagePicker(controller: self.imagePickerController!, source: .savedPhotosAlbum)
            }))
        }
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel))
        self.present(alert, animated: true, completion: nil)
        print("1")
    }
    
    internal func presentImagePicker(controller: UIImagePickerController, source: UIImagePickerController.SourceType) {
        controller.delegate = self
        controller.sourceType = source
        self.present(controller, animated: true)
    }
}

extension CreateProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = profileTableView.dequeueReusableCell(withIdentifier: reusableProfileCell, for: indexPath) as? CreateProfileTableViewCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        
        cell.titleLabel.text = titles[indexPath.row].title
        cell.inputTextField.placeholder = titles[indexPath.row].placeholder
        
        cell.expandBtn.setImage(titles[indexPath.row].addContent, for: .normal)
        return cell
    }
}

extension CreateProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return self.imagePickerControllerDidCancel(picker)
        }
        
        avatarImageView.image = selectedImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

