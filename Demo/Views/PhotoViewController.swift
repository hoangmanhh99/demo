//
//  PhotoViewController.swift
//  Demo
//
//  Created by MacOS on 22/03/2021.
//

import UIKit

class PhotoViewController: UIViewController {
    @IBOutlet weak var photoCollectionView: UICollectionView!
    
    var photos: [Photo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initData()
        
        let nib = UINib(nibName: "PhotoCollectionViewCell", bundle: .main)
        photoCollectionView.register(nib, forCellWithReuseIdentifier: PhotoCollectionViewCell.identifier)
    }
    	
    func initData() {
        APIClient.getListPhotos { (items, _) in
            guard let items = items as? [Photo] else { return }
            self.photos.removeAll()
            self.photos.append(contentsOf: items)
            DispatchQueue.main.async {
                self.photoCollectionView.reloadData()
            }
        }
    }
}

extension PhotoViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as? PhotoCollectionViewCell else { return UICollectionViewCell() }
        cell.configureWith(self.photos[indexPath.row])
        
        return cell
    }
    
    
}
