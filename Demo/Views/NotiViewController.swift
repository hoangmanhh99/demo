//
//  NotiViewController.swift
//  Demo
//
//  Created by MacOS on 03/03/2021.
//

import UIKit

enum SectionType {
    case general
    case directMessages
    case groupChats
    case callSettings
}

enum Title {
    case enableNotifications
    case doNotDisturb
    case socialPost
    case taskAssigns
    case birthdayMentions
    case newMessageAlert
    case notificationPanel
    case soundOn
    case vibrationOn
    case ringtone
}

extension Title {
    var title: String? {
        switch self {
        case .enableNotifications:
            return "Enable Notifications"
        case .doNotDisturb:
            return "Do Not Disturb"
        case .socialPost:
            return "Social Post Mentions"
        case .taskAssigns:
            return "Task Assigns"
        case .birthdayMentions:
            return "Birthday Mentions"
        case .newMessageAlert:
            return "New Message Alert"
        case .notificationPanel:
            return "Notification Panel"
        case .soundOn:
            return "Sound On"
        case .vibrationOn:
            return "Vibration On"
        case .ringtone:
            return "Ringtone"
        }
    }
    
    var subTitle: String? {
        switch self {
        case .doNotDisturb:
            return "During this time, you will not receive  any notifications from this channel"
        case .socialPost:
            return "Any mentions from Social Post"
        case .taskAssigns:
            return "When assigned tasks from To Do List, etc"
        case .birthdayMentions:
            return "Be notified during contacts'birthdays"
        case .ringtone:
            return "Default"
        default:
            return nil
        }
    }
    
    var time: String? {
        switch self {
        case .doNotDisturb:
            return "From 9:00am, To 5:00pm"
        default:
            return nil
        }
    }
}

extension SectionType {
    var items: [Title] {
        switch self {
        case .general:
            return [.enableNotifications, .doNotDisturb, .socialPost, .taskAssigns, .birthdayMentions]
        case .directMessages:
            return [.newMessageAlert, .notificationPanel, .soundOn, .vibrationOn]
        case .groupChats:
            return [.newMessageAlert, .notificationPanel, .soundOn, .vibrationOn]
        case .callSettings:
            return [.ringtone, .vibrationOn]
        }
    }
    
    var title: String {
        switch self {
        case .general:
            return "General"
        case .directMessages:
            return "Direct Messages"
        case .groupChats:
            return "Group Chats"
        case .callSettings:
            return "Call Settings"
        }
    }
}

class NotiViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var notiTableView: UITableView!
    
    private var sections: [SectionType] = [.general, .directMessages, .groupChats, .callSettings]
    
    private var values: [Title: Bool] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "NotiTableViewCell", bundle: .main)
        notiTableView.register(nib, forCellReuseIdentifier: reusableCell)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = notiTableView.dequeueReusableCell(withIdentifier: reusableCell, for: indexPath) as? NotiTableViewCell else { return UITableViewCell()}
        cell.selectionStyle = .none
        let title = sections[indexPath.section].items[indexPath.row]
        cell.titleNotiLabel.text = title.title
        cell.contentNotiLabel.text = title.subTitle
        cell.timeLabel.text = title.time
        
        if let value = self.values[title] {
            cell.onOffSwitch.setOn(value, animated: false)
        } else {
            cell.onOffSwitch.setOn(true, animated: false)
        }
        
        cell.delegate = self
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = UIColor.white
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor(rgb: 0x595959)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func updateSwitch(_ state: String) {
        print(state)
    }
}

extension NotiViewController: SwitchDelegate {
    func updateSwitch(_ sender: NotiTableViewCell, _ valueChanged: Bool) {
        guard let indexPath = self.notiTableView.indexPath(for: sender) else { return }
        let title = sections[indexPath.section].items[indexPath.row]
        self.values[title] = valueChanged
    }
}
